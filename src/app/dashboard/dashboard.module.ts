import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {DashboardRoutingModule} from './dashboard-routing.module';

import {DashboardComponent} from './dashboard/dashboard.component';
import {AddCompanyComponent} from './dashboard/add-company/add-company.component';

@NgModule({
  declarations: [DashboardComponent, AddCompanyComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule {
}
