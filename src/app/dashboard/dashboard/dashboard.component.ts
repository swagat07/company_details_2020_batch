import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public showPopup = false;

  public companyList = [
    {
      id: 1,
      name: 'Facebook',
      description: 'A lump-sum payment is an often large sum that is paid in one single payment instead of broken up into installments. ' +
        'It is also known as a bullet repayment when dealing with a loan. They are sometimes associated with pension plans and other ' +
        'retirement vehicles, such as 401k accounts, where retirees accept a smaller upfront lump-sum payment rather than a larger sum ' +
        'paid out over time. These are often paid out in the event of debentures.',
      country: 1,
      themeColor: '#0000FF',
      createdDate: ''
    },
    {
      id: 2,
      name: 'Yahoo',
      description: 'A lump-sum payment is an often large sum that is paid in one single payment instead of broken up into installments. ' +
        'It is also known as a bullet repayment when dealing with a loan. They are sometimes associated with pension plans and other ' +
        'retirement vehicles, such as 401k accounts, where retirees accept a smaller upfront lump-sum payment rather than a larger sum ' +
        'paid out over time. These are often paid out in the event of debentures.',
      country: 2,
      themeColor: '#FF0000',
      createdDate: ''
    }
  ];
  private selectedCompany;

  constructor() {
  }

  ngOnInit() {
  }

  companyData(id: number): void {
    if (id) {
      this.selectedCompany = this.companyList.find(company => company.id === id);
    } else {
      this.selectedCompany = null;
    }
  }

  updateCompany(data): void {
    const companyData: any = {
      ...data,
      id: this.companyList.length + 1
    };

    this.companyList.push(companyData);
  }

}
