import {Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.css']
})
export class AddCompanyComponent implements OnInit {

  @ViewChild('popup', {static: true}) popup: TemplateRef<any>;

  @Output() outputOfAddCompany = new EventEmitter();

  @Input() set getCompanyData(value) {
    this.formInit(value);
  }

  public form: FormGroup;

  public countryList = [
    {
      id: 1,
      name: 'India'
    },
    {
      id: 2,
      name: 'USA'
    },
    {
      id: 3,
      name: 'NZ'
    }
  ];

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {
  }

  ngOnInit() {
    this.openPopup(this.popup);
  }

  formInit(data): void {
    this.form = this.fb.group({
      name: [data ? data.name : '', Validators.compose([Validators.required, Validators.minLength(5)])],
      description: [data ? data.description : '', Validators.required],
      country: [data ? data.country : '', Validators.required],
      themeColor: [data ? data.themeColor : '', Validators.required]
    });
  }

  openPopup(content: TemplateRef<any>): void {
    this.modalService.open(content, {backdrop: 'static', centered: true});
  }

  closePopup(): void {
    this.outputOfAddCompany.emit();
  }

  addCompanyData(): void {
    this.outputOfAddCompany.emit(this.form.value);
  }

}
